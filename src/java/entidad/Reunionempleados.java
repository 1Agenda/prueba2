/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entidad;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Daftzero
 */
@Entity
@Table(name = "reunionempleados", catalog = "prueba", schema = "")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Reunionempleados.findAll", query = "SELECT r FROM Reunionempleados r"),
    @NamedQuery(name = "Reunionempleados.findByIdReunionEmpleados", query = "SELECT r FROM Reunionempleados r WHERE r.idReunionEmpleados = :idReunionEmpleados")})
public class Reunionempleados implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idReunionEmpleados")
    private Integer idReunionEmpleados;
    @JoinColumn(name = "idEmpleados", referencedColumnName = "idEmpleados")
    @ManyToOne(optional = false, fetch = FetchType.EAGER)
    private Empleados idEmpleados;
    @JoinColumn(name = "idreunion", referencedColumnName = "idReunion")
    @ManyToOne(optional = false, fetch = FetchType.EAGER)
    private Reunion idreunion;

    public Reunionempleados() {
    }

    public Reunionempleados(Integer idReunionEmpleados) {
        this.idReunionEmpleados = idReunionEmpleados;
    }

    public Integer getIdReunionEmpleados() {
        return idReunionEmpleados;
    }

    public void setIdReunionEmpleados(Integer idReunionEmpleados) {
        this.idReunionEmpleados = idReunionEmpleados;
    }

    public Empleados getIdEmpleados() {
        return idEmpleados;
    }

    public void setIdEmpleados(Empleados idEmpleados) {
        this.idEmpleados = idEmpleados;
    }

    public Reunion getIdreunion() {
        return idreunion;
    }

    public void setIdreunion(Reunion idreunion) {
        this.idreunion = idreunion;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idReunionEmpleados != null ? idReunionEmpleados.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Reunionempleados)) {
            return false;
        }
        Reunionempleados other = (Reunionempleados) object;
        if ((this.idReunionEmpleados == null && other.idReunionEmpleados != null) || (this.idReunionEmpleados != null && !this.idReunionEmpleados.equals(other.idReunionEmpleados))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entidad.Reunionempleados[ idReunionEmpleados=" + idReunionEmpleados + " ]";
    }
    
}
