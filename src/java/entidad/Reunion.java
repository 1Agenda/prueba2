/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entidad;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Daftzero
 */
@Entity
@Table(name = "reunion", catalog = "prueba", schema = "")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Reunion.findAll", query = "SELECT r FROM Reunion r"),
    @NamedQuery(name = "Reunion.findByIdReunion", query = "SELECT r FROM Reunion r WHERE r.idReunion = :idReunion")})
public class Reunion implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idReunion")
    private Integer idReunion;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idReunion", fetch = FetchType.EAGER)
    private List<Reunionproveedores> reunionproveedoresList;
    @JoinColumn(name = "idAgenda", referencedColumnName = "idAgenda")
    @ManyToOne(optional = false, fetch = FetchType.EAGER)
    private Agenda idAgenda;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idreunion", fetch = FetchType.EAGER)
    private List<Reunionempleados> reunionempleadosList;

    public Reunion() {
    }

    public Reunion(Integer idReunion) {
        this.idReunion = idReunion;
    }

    public Integer getIdReunion() {
        return idReunion;
    }

    public void setIdReunion(Integer idReunion) {
        this.idReunion = idReunion;
    }

    @XmlTransient
    public List<Reunionproveedores> getReunionproveedoresList() {
        return reunionproveedoresList;
    }

    public void setReunionproveedoresList(List<Reunionproveedores> reunionproveedoresList) {
        this.reunionproveedoresList = reunionproveedoresList;
    }

    public Agenda getIdAgenda() {
        return idAgenda;
    }

    public void setIdAgenda(Agenda idAgenda) {
        this.idAgenda = idAgenda;
    }

    @XmlTransient
    public List<Reunionempleados> getReunionempleadosList() {
        return reunionempleadosList;
    }

    public void setReunionempleadosList(List<Reunionempleados> reunionempleadosList) {
        this.reunionempleadosList = reunionempleadosList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idReunion != null ? idReunion.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Reunion)) {
            return false;
        }
        Reunion other = (Reunion) object;
        if ((this.idReunion == null && other.idReunion != null) || (this.idReunion != null && !this.idReunion.equals(other.idReunion))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entidad.Reunion[ idReunion=" + idReunion + " ]";
    }
    
}
