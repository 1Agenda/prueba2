/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entidad;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Daftzero
 */
@Entity
@Table(name = "eventoparticipantes", catalog = "prueba", schema = "")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Eventoparticipantes.findAll", query = "SELECT e FROM Eventoparticipantes e"),
    @NamedQuery(name = "Eventoparticipantes.findByIdEventoParticipantes", query = "SELECT e FROM Eventoparticipantes e WHERE e.idEventoParticipantes = :idEventoParticipantes"),
    @NamedQuery(name = "Eventoparticipantes.findByConfirmado", query = "SELECT e FROM Eventoparticipantes e WHERE e.confirmado = :confirmado")})
public class Eventoparticipantes implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idEventoParticipantes")
    private Integer idEventoParticipantes;
    @Basic(optional = false)
    @NotNull
    @Column(name = "confirmado")
    private boolean confirmado;
    @JoinColumn(name = "idAutoRegistro", referencedColumnName = "idAutoRegistro")
    @ManyToOne(optional = false, fetch = FetchType.EAGER)
    private AutoRegistro idAutoRegistro;
    @JoinColumn(name = "idEvento", referencedColumnName = "idevento")
    @ManyToOne(optional = false, fetch = FetchType.EAGER)
    private Evento idEvento;

    public Eventoparticipantes() {
    }

    public Eventoparticipantes(Integer idEventoParticipantes) {
        this.idEventoParticipantes = idEventoParticipantes;
    }

    public Eventoparticipantes(Integer idEventoParticipantes, boolean confirmado) {
        this.idEventoParticipantes = idEventoParticipantes;
        this.confirmado = confirmado;
    }

    public Integer getIdEventoParticipantes() {
        return idEventoParticipantes;
    }

    public void setIdEventoParticipantes(Integer idEventoParticipantes) {
        this.idEventoParticipantes = idEventoParticipantes;
    }

    public boolean getConfirmado() {
        return confirmado;
    }

    public void setConfirmado(boolean confirmado) {
        this.confirmado = confirmado;
    }

    public AutoRegistro getIdAutoRegistro() {
        return idAutoRegistro;
    }

    public void setIdAutoRegistro(AutoRegistro idAutoRegistro) {
        this.idAutoRegistro = idAutoRegistro;
    }

    public Evento getIdEvento() {
        return idEvento;
    }

    public void setIdEvento(Evento idEvento) {
        this.idEvento = idEvento;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idEventoParticipantes != null ? idEventoParticipantes.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Eventoparticipantes)) {
            return false;
        }
        Eventoparticipantes other = (Eventoparticipantes) object;
        if ((this.idEventoParticipantes == null && other.idEventoParticipantes != null) || (this.idEventoParticipantes != null && !this.idEventoParticipantes.equals(other.idEventoParticipantes))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entidad.Eventoparticipantes[ idEventoParticipantes=" + idEventoParticipantes + " ]";
    }
    
}
