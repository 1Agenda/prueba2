/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sesion;

import entidad.Eventoparticipantes;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author Daftzero
 */
@Stateless
public class EventoparticipantesFacade extends AbstractFacade<Eventoparticipantes> {
    @PersistenceContext(unitName = "WebApplication7PU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public EventoparticipantesFacade() {
        super(Eventoparticipantes.class);
    }
    
}
